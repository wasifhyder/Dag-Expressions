from datetime import datetime
from flask import session, flash, redirect, url_for, render_template
from .import main
from .forms import ExpressionForm
from ..models import Expression
from .. import db

@main.route('/', methods=['GET', 'POST'])
def index():
    form = ExpressionForm()
    if form.validate_on_submit():
        expression = Expression(form.expression.data)
        db.session.add(expression)
        db.session.commit()
        form.expression.data = ''
        flash("Expression posted!")
        return redirect(url_for('.index'))
    return render_template('index.html',
                           expressions=Expression.query.all(),
                           form=form,
                           current_time=datetime.utcnow())
