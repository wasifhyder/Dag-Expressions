from flask.ext.wtf import Form
from wtforms import TextAreaField, SubmitField
from wtforms.validators import Required


class ExpressionForm(Form):
    expression = TextAreaField("What would you like to express?", validators=[Required()])
    submit = SubmitField('Submit')