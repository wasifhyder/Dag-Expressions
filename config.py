import os
basedir = os.path.abspath(os.path.dirname(__file__))


# Inheritance Pattern for laying out configurations for different
# deployment modes.
# -------------------------------------------------------------------
# Base configuration class
class Config:
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'hard to gues string'
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True

    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(Config):
    'Development environment specific config'
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('DEV_DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, 'data-dev.sqlite')


class TestingConfig(Config):
    'Testing specific config'
    TESTING = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('TEST_DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, 'data-test.sqlite')


class ProductionConfig(Config):
    if os.environ.get('DATABASE_URL') is None:
        SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
                                  'sqlite:///' + os.path.join(basedir, 'data.sqlite')
    else:
        SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']
    # Secret Key - stored in a separate file for security reasons
    # SECRET_KEY = open('/path/to/secret/file').read()


config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,

    'default': DevelopmentConfig
}
